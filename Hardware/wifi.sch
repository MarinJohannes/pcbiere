EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP-07 U2
U 1 1 634B45F3
P 5150 2600
F 0 "U2" H 5500 3500 50  0000 C CNN
F 1 "ESP-07" H 5500 3400 50  0000 C CNN
F 2 "RF_Module:ESP-07" H 5150 2600 50  0001 C CNN
F 3 "http://wiki.ai-thinker.com/_media/esp8266/esp8266_series_modules_user_manual_v1.1.pdf" H 4800 2700 50  0001 C CNN
	1    5150 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 634B5410
P 5150 3300
F 0 "#PWR033" H 5150 3050 50  0001 C CNN
F 1 "GND" H 5155 3127 50  0000 C CNN
F 2 "" H 5150 3300 50  0001 C CNN
F 3 "" H 5150 3300 50  0001 C CNN
	1    5150 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR032
U 1 1 634B5906
P 5150 1800
F 0 "#PWR032" H 5150 1650 50  0001 C CNN
F 1 "+3.3V" H 5165 1973 50  0000 C CNN
F 2 "" H 5150 1800 50  0001 C CNN
F 3 "" H 5150 1800 50  0001 C CNN
	1    5150 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 634B6A02
P 5050 1000
F 0 "C8" H 5142 1046 50  0000 L CNN
F 1 "10u" H 5142 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5050 1000 50  0001 C CNN
F 3 "~" H 5050 1000 50  0001 C CNN
	1    5050 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR030
U 1 1 634B720E
P 5050 900
F 0 "#PWR030" H 5050 750 50  0001 C CNN
F 1 "+3.3V" H 5065 1073 50  0000 C CNN
F 2 "" H 5050 900 50  0001 C CNN
F 3 "" H 5050 900 50  0001 C CNN
	1    5050 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR031
U 1 1 634B7362
P 5050 1100
F 0 "#PWR031" H 5050 850 50  0001 C CNN
F 1 "GND" H 5055 927 50  0000 C CNN
F 2 "" H 5050 1100 50  0001 C CNN
F 3 "" H 5050 1100 50  0001 C CNN
	1    5050 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 634B807B
P 5400 1000
F 0 "C9" H 5492 1046 50  0000 L CNN
F 1 "100n" H 5492 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5400 1000 50  0001 C CNN
F 3 "~" H 5400 1000 50  0001 C CNN
	1    5400 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR034
U 1 1 634B8081
P 5400 900
F 0 "#PWR034" H 5400 750 50  0001 C CNN
F 1 "+3.3V" H 5415 1073 50  0000 C CNN
F 2 "" H 5400 900 50  0001 C CNN
F 3 "" H 5400 900 50  0001 C CNN
	1    5400 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR035
U 1 1 634B8087
P 5400 1100
F 0 "#PWR035" H 5400 850 50  0001 C CNN
F 1 "GND" H 5405 927 50  0000 C CNN
F 2 "" H 5400 1100 50  0001 C CNN
F 3 "" H 5400 1100 50  0001 C CNN
	1    5400 1100
	1    0    0    -1  
$EndComp
Text HLabel 5750 2300 2    50   Input ~ 0
ESP_RX
Text HLabel 5750 2100 2    50   Output ~ 0
ESP_TX
NoConn ~ 4550 2000
NoConn ~ 4550 2200
NoConn ~ 4550 2400
NoConn ~ 5750 3000
NoConn ~ 5750 2800
NoConn ~ 5750 2600
NoConn ~ 5750 2500
NoConn ~ 5750 2400
NoConn ~ 5750 2200
NoConn ~ 5750 2000
Text HLabel 5750 2900 2    50   BiDi ~ 0
ESP_RTS
Text HLabel 5750 2700 2    50   BiDi ~ 0
ESP_CTS
$EndSCHEMATC
