# PCBiere

![Image3D](Hardware/pic.png?raw=true "Modélisation PCBière 3D")

## Fonctionnement
Machine à bière controllée par une STM32f4 et une carte WiFi (via un futur interface python).  

- Dans un état automatique, la LED représentant l'étape actuelle du processus est variable, contrairement à celles des étapes terminées (allumée) ou non commencées (éteintes). 
- Dans le mode manu, les LEDs étapes clignotent.
- Basculer d'un mode à l'autre se fait via un appui prolongé d'au moins 2s sur le bouton PAUSE, puis un relachement.
  
- Pour commencer, appuyer brièvement sur le bouton pause. La machine doit passer en mode INITIALISATION. Ajouter l'eau et le malt. Une fois fait, appuyer une nouvelle fois sur le bouton PAUSE.  
- L'eau va automatiquement monter en température pour suivre les paliers. Une fois terminée, la LED devient fixe et la LED houblonnage devient variable.  
- L'eau va alors être chauffée jusqu'à bouillir. Une fois que c'est fait, la led houblonnage va clignoter quand il faut ajouter du houblon, et être variable quand elle chauffe. Chaque ajout de houblon doit être suivi d'un appui sur le bouton PAUSE.
- Une fois terminée, on attend que l'eau soit à bonne température pour la levure, puis une fois que c'est fait, toutes les LEDs sont fixes.


## Fonctionnalitées
- Se branche directement sur le 230V
- Relais directment intégrés
- 2 résistances chauffantes, 2 pompes, 1 relais, 2 sorties 12V : Attention total dependant du disjoncteur, mais ne pas depasser 25A


## ToDo
- Souder un fil entre la patte du bouton Pause et la sortie PB1 (à corriger si éventuellement d'autres versions) 
- Capteurs de température : https://controllerstech.com/ds18b20-and-stm32/
- Acheter une pompe?
- Trouver une visu des temps restants
- Programmer la carte en dur pour tester le process
- Boitier à l'impression 3D  
- Intégrer la communication WiFi  
- Interface python pour choisir une recette, lancer et suivre l'avancement
- Configurer l'IP de la carte et le point d'accès? Reset en AP via appuie long, faisant arriver sur une page web demandant ssid/pass + dhcp/ip 
