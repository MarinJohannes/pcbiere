/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "config.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef struct  {
	uint8_t brewingType, heaters[3], pumps[3], tempSensors[3], tempSensorsTypes[2];
} MACHINE_CONFIG_t;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void MX_FREERTOS_Init(void);
void delay(volatile uint32_t us);
void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
void Set_Pin_Output(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
void brewingLoop(void);
void LEDWifiLoop(enum stateWifi * WifiSteps);
void SaveConfig(MACHINE_CONFIG_t * config);
void LoadConfig(MACHINE_CONFIG_t * config);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define IN_OUT2_Pin GPIO_PIN_13
#define IN_OUT2_GPIO_Port GPIOC
#define IN_H1_Pin GPIO_PIN_14
#define IN_H1_GPIO_Port GPIOC
#define IN_H2_Pin GPIO_PIN_15
#define IN_H2_GPIO_Port GPIOC
#define SW_H1_Pin GPIO_PIN_0
#define SW_H1_GPIO_Port GPIOC
#define SW_H2_Pin GPIO_PIN_1
#define SW_H2_GPIO_Port GPIOC
#define SW_P1_Pin GPIO_PIN_2
#define SW_P1_GPIO_Port GPIOC
#define SW_P2_Pin GPIO_PIN_3
#define SW_P2_GPIO_Port GPIOC
#define TEMP1_Pin GPIO_PIN_0
#define TEMP1_GPIO_Port GPIOA
#define TEMP2_Pin GPIO_PIN_1
#define TEMP2_GPIO_Port GPIOA
#define SW_MODE_Pin GPIO_PIN_4
#define SW_MODE_GPIO_Port GPIOA
#define WIFI_RST_Pin GPIO_PIN_5
#define WIFI_RST_GPIO_Port GPIOA
#define SW_P3_Pin GPIO_PIN_4
#define SW_P3_GPIO_Port GPIOC
#define SW_PAUSE_Pin GPIO_PIN_1
#define SW_PAUSE_GPIO_Port GPIOB
#define LED_REFROID_Pin GPIO_PIN_12
#define LED_REFROID_GPIO_Port GPIOB
#define LED_HOUB_Pin GPIO_PIN_13
#define LED_HOUB_GPIO_Port GPIOB
#define LED_EMPAT_Pin GPIO_PIN_14
#define LED_EMPAT_GPIO_Port GPIOB
#define LED_INIT_Pin GPIO_PIN_15
#define LED_INIT_GPIO_Port GPIOB
#define SW_O1_Pin GPIO_PIN_6
#define SW_O1_GPIO_Port GPIOC
#define SW_O2_Pin GPIO_PIN_7
#define SW_O2_GPIO_Port GPIOC
#define IN_OUT1_Pin GPIO_PIN_8
#define IN_OUT1_GPIO_Port GPIOC
#define IN_OUT2C9_Pin GPIO_PIN_9
#define IN_OUT2C9_GPIO_Port GPIOC
#define LED_WIFI_Pin GPIO_PIN_15
#define LED_WIFI_GPIO_Port GPIOA
#define IN_P1_Pin GPIO_PIN_10
#define IN_P1_GPIO_Port GPIOC
#define IN_P2_Pin GPIO_PIN_11
#define IN_P2_GPIO_Port GPIOC
#define IN_P3_Pin GPIO_PIN_12
#define IN_P3_GPIO_Port GPIOC
#define DS18B20_Pin GPIO_PIN_3
#define DS18B20_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
