/*
 * tank.h
 *
 *  Created on: 28 nov. 2022
 *      Author: MarinJ
 */

#ifndef INC_TANK_H_
#define INC_TANK_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "heater.h"
#include "sensor.h"
#include "pump.h"

class Tank {
	private:

	public:
		Heater * heater;
		Pump * pump;
		TempSensor * sensor;
		Tank(Heater * h, TempSensor * s, Pump * p);
};

#endif /* INC_TANK_H_ */
