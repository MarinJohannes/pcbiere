/*
 * communication.h
 *
 *  Created on: 1 nov. 2022
 *      Author: Marin
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef INC_COMMUNICATION_H_
#define INC_COMMUNICATION_H_



enum HeartBeatStatus {
	ST_INACTIVE,
	ST_MANUEL,
	ST_INIT,
	ST_EMPAT,
	ST_HOUB,
	ST_REFROI
};

char * GenerateHearbeat(void);
void AnalyseCommand(char * str);


#endif /* INC_COMMUNICATION_H_ */

#ifdef __cplusplus
}
#endif
