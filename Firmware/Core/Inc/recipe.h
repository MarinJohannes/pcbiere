/*
 * receipe.h
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#ifndef INC_RECIPE_H_
#define INC_RECIPE_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "main.h"
#include "hops.h"

typedef struct {
	uint8_t temp, duree, done;
} EMPAT_STEP;

typedef struct {
	enum HOP_LIST hop;
	uint8_t duree;
} HOP;

enum HOP_STEP {
	BOILING,
	FIRST_BOIL,
	WAITING_FOR_HOP
};

class Recipe {
	private:
		EMPAT_STEP Paliers[5];
		HOP Hops[10];
		uint32_t hop_duration, hopSteps[10];
		uint8_t n_step, i_step, h_step, yeast_temp, n_hop;

	public:
		enum HOP_STEP hopStep;
		Recipe(void);
		void AddMaltStep(uint8_t temp, uint8_t duree);
		void NextMaltStep(void);
		uint8_t IsMaltStepDone(void);
		uint8_t IsMaltDone(void);
		uint8_t GetMaltStepTemp(void);
		uint8_t GetMaltStepDuration(void);
		void AddHop(enum HOP_LIST hop, uint8_t duree);
		void CalculateHops(void);
		void NextHopStep(void);
		uint8_t GetHopStepDuration(void);
		uint8_t IsHopDone(void);
		void SetYeastTemp(uint8_t temp);
		uint8_t GetYeastTemp(void);
};


#endif /* INC_RECIPE_H_ */
