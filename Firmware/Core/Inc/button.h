/*
 * button.h
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#ifndef INC_BUTTON_H_
#define INC_BUTTON_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"

class Button {
	private:
		GPIO_TypeDef * reg;
		uint16_t mem_pushed;
		uint16_t pin, cpt;
		uint8_t rising;

	public:
		Button(GPIO_TypeDef * reg, uint16_t pin);
		void Read(void);
		uint8_t IsPushed(void);
		uint16_t HowLong(void);
		uint8_t isRisingEdge(void);
		void Reset(void);
};

void ButtonLoop(void);

#endif /* INC_BUTTON_H_ */
