/*
 * heater.h
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#ifndef INC_PUMP_H_
#define INC_PUMP_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "main.h"

class Pump {
	private:
		GPIO_TypeDef * reg;
		uint32_t duree;
		uint16_t pin, cpt;
		uint8_t state;
		uint8_t unpause_state, mem_pause;

	public:
		uint8_t isOn;
		Pump(GPIO_TypeDef * reg, uint16_t pin);
		void ON(void);
		void OFF(void);
		void Toggle(void);
		void Play(void);
};

void PumpLoop(void);

#endif /* INC_HEATER_H_ */
