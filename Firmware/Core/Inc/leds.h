/*
 * leds.h
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#ifndef SRC_LEDS_H_
#define SRC_LEDS_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "main.h"

class LED {
	private:
		GPIO_TypeDef * reg;
		uint16_t pin, cpt, T;
		uint8_t state, alpha;

	public:
		LED(GPIO_TypeDef * reg, uint16_t pin);
		void Play(void);
		void ON(void);
		void OFF(void);
		void Blink(float tau, uint16_t T);
		void Sync(void);
		void Breath(void);
};

void LEDLoop(void);

#endif /* SRC_LEDS_H_ */
