/*
 * sensor.h
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#ifndef INC_SENSOR_H_
#define INC_SENSOR_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "main.h"

enum TEMPERATURE_SENSOR_TYPE {
	PT100 = 0,
	KTY83,
	KTY81,
	DS18B20,
	NTC_5K,
	NTC_10K
};

class TempSensor {
	private:
		uint32_t analog, cpt=0;
		GPIO_TypeDef * reg;
		uint16_t pin;
		float pt100_alpha;
		uint8_t steps=0;

	public:
		enum TEMPERATURE_SENSOR_TYPE type;
		float value;
		TempSensor(GPIO_TypeDef * reg, uint16_t pin, enum TEMPERATURE_SENSOR_TYPE type);
		float GetValue(void);
		void SetValue(uint32_t analog);
		void StartConv(void);
		void ChangeType(enum TEMPERATURE_SENSOR_TYPE t);
		void ChangeType(uint8_t t);
};



#endif /* INC_SENSOR_H_ */
