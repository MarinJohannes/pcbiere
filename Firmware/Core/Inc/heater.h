/*
 * heater.h
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#ifndef INC_HEATER_H_
#define INC_HEATER_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "main.h"
#include "sensor.h"

class Heater {
	private:
		TempSensor * sensor;
		GPIO_TypeDef * reg;
		uint32_t duree;
		uint16_t pin, cpt;
		uint8_t target_temp, state, reached, done;
		uint8_t unpause_state, mem_pause;

	public:
		uint8_t isOn;
		Heater(GPIO_TypeDef * reg, uint16_t pin, TempSensor * sensor);
		void AssignSensor(TempSensor * sensor);
		void ON(void);
		void OFF(void);
		void Resume(void);
		void Toggle(void);
		void Play(void);
		uint8_t IsDone(void);
		void SetTarget(uint8_t target, uint8_t duree_minutes);

};

void HeaterLoop(void);

#endif /* INC_HEATER_H_ */
