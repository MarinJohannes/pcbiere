/*
 * brew.h
 *
 *  Created on: 28 nov. 2022
 *      Author: MarinJ
 */

#ifndef INC_BREW_H_
#define INC_BREW_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "tank.h"

enum BREW_TYPE {
	BIAB=0,
	HERMS
};

enum BREW_STATUS {
	BS_STARTED=0,
	BS_STOPPED,
	BS_PAUSED
};

class BrewSystem {
	private:
		enum stateMachine state;
		Tank * MLT, * HLT, * BK;

	public:
		enum BREW_STATUS brewStatus = BS_STOPPED;
		uint8_t Next=0, PAUSE=0;
		enum BREW_TYPE brewType;
		BrewSystem(Tank * Tank);
		BrewSystem(Tank * MLT, Tank * HLT, Tank * BK);
		void Loop(void);
};

#endif /* INC_BREW_H_ */
