/*
 * output.h
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#ifndef INC_OUTPUT_H_
#define INC_PUMP_H_

#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "main.h"

class Output {
	private:
		GPIO_TypeDef * reg;
		uint32_t duree;
		uint16_t pin, cpt;
		uint8_t state;

	public:
		uint8_t isOn;
		Output(GPIO_TypeDef * reg, uint16_t pin);
		void ON(void);
		void OFF(void);
		void Toggle(void);
		void Play(void);
};

void OutputLoop(void);

#endif
