/*
 * output.cpp
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#include "output.h"

extern Output O1, O2;

void OutputLoop(void) {
	O1.Play();
	O2.Play();
}

Output::Output(GPIO_TypeDef * reg, uint16_t pin) {
	this->pin = pin;
	this->reg = reg;
	this->cpt = 0;
	this->state = 0;
	this->isOn = 0;
}

void Output::ON(void) {
	this->state = 1;
}

void Output::OFF(void) {
	this->state = 0;
}

void Output::Play(void) {
	switch (this->state) {
		case 0:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
			this->isOn = 0;
			break;

		case 1:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_SET);
			this->isOn = 1;
			break;
	}
}

void Output::Toggle(void) {
	if (this->state > 0)
		this->state = 0;
	else
		this->state = 1;
}
