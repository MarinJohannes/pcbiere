/*
 * button.cpp
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#include "button.h"
#include "main.h"

extern Button BP_H1, BP_H2, BP_P1, BP_P2, BP_P3, BP_MODE, BP_O1, BP_O2, BP_PAUSE;

void ButtonLoop(void) {
	// Toutes les ms, à mettre dans TIM4
	BP_H1.Read();
	BP_H2.Read();
	BP_P1.Read();
	BP_P2.Read();
	BP_P3.Read();
	BP_MODE.Read();
	BP_O1.Read();
	BP_O2.Read();
	BP_PAUSE.Read();
}

Button::Button(GPIO_TypeDef * reg, uint16_t pin) {
	this->pin = pin;
	this->reg = reg;
	this->cpt = 0;
	this->rising = 0;
}

void Button::Read(void) {
	if (HAL_GPIO_ReadPin(this->reg, this->pin) == GPIO_PIN_RESET) {
		this->cpt++;
		this->rising = 0;
	}
	else {
		if (cpt > 50) {
			this->rising = 1;
		}
		this->cpt = 0;
	}
}

uint8_t Button::IsPushed(void) {
	uint8_t pushed = 0;
	if (this->cpt > 50) {
		pushed = 1;
	}
	return pushed;
}

uint8_t Button::isRisingEdge(void) {
	if (this->rising) {
		this->rising = 0;
		return 1;
	}
	return 0;
}

uint16_t Button::HowLong(void) {
	return this->cpt;
}

void Button::Reset(void) {
	this->cpt=0;
	this->rising = 0;
}
