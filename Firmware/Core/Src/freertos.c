/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "communication.h"
#include "usart.h"
#include <string.h>
#include "config.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
uint8_t msg_list[PARSER_MESSAGE_LIST_SIZE][PARSER_MESSAGE_SIZE];
extern uint8_t buffer[UART_DMA_BUFFER_SIZE];

extern enum stateMachine sMachine;
extern uint8_t PAUSE;
extern UART_HandleTypeDef huart1;
extern uint8_t AT_search_items[5][PARSER_MESSAGE_SIZE];

static volatile int8_t ITEM_FOUND=-1;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// DECLARATIONS /////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CAPTEURS
/*
extern TempSensor Temp1, Temp2;
// HEATER
extern Heater H1, H2;
// POMPES
extern Pump P1, P2, P3;
// Sorties 12V
extern Output O1, O2;
// LEDs
extern LED LED_INIT, LED_EMPAT, LED_HOUB, LED_REFROID, LED_WIFI;
// Boutons
extern Button BP_H1, BP_H2, BP_P1, BP_P2, BP_P3, BP_MODE, BP_O1, BP_O2, BP_PAUSE;
// Recipe
extern Recipe Recipe;
*/
/* USER CODE END Variables */
osThreadId uartParserTaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
osThreadId brewingTaskHandle, uartReceptHandle, AtProcessHandle;
/* USER CODE END FunctionPrototypes */

void UARTParser(void const * argument);
void brewingTask(void const * argument);
void AtProcessingTask(void const * argument);
void UartReceptTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* GetTimerTaskMemory prototype (linked to static allocation support) */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* USER CODE BEGIN GET_TIMER_TASK_MEMORY */
static StaticTask_t xTimerTaskTCBBuffer;
static StackType_t xTimerStack[configTIMER_TASK_STACK_DEPTH];

void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize )
{
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCBBuffer;
  *ppxTimerTaskStackBuffer = &xTimerStack[0];
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
  /* place for user code */
}
/* USER CODE END GET_TIMER_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of uartParserTask */
  osThreadDef(uartParserTask, UARTParser, osPriorityNormal, 0, 512);
  uartParserTaskHandle = osThreadCreate(osThread(uartParserTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  osThreadDef(brewingTaskHandler, brewingTask, osPriorityBelowNormal, 0, 256);
  brewingTaskHandle = osThreadCreate(osThread(brewingTaskHandler), NULL);

  osThreadDef(uartReceptHandler, UartReceptTask, osPriorityLow, 0, 512);
  uartReceptHandle = osThreadCreate(osThread(uartReceptHandler), NULL);

 // osThreadDef(AtProcessHandler, AtProcessingTask, osPriorityLow, 0, 256);
  //ardAtProcessHandle = osThreadCreate(osThread(AtProcessHandler), NULL);
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_UARTParser */
/**
  * @brief  Function implementing the uartParserTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_UARTParser */
void UARTParser(void const * argument)
{
  /* USER CODE BEGIN UARTParser */
	size_t dma_head = 0, dma_tail = 0;
	size_t cur_msg_sz = 0;
	size_t cur_msg = 0;
	uint8_t found = 0;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	HAL_UART_Receive_DMA(&huart1, buffer, UART_DMA_BUFFER_SIZE);

	/* Infinite loop */
	for(;;) {
		do {
			if (huart1.hdmarx->State != HAL_DMA_STATE_BUSY)
				HAL_UART_Receive_DMA(&huart1, buffer, UART_DMA_BUFFER_SIZE);
			__disable_irq();
			//dma_tail = UART_DMA_BUFFER_SIZE - huart1.hdmarx->Instance->CNDTR;
			dma_tail = UART_DMA_BUFFER_SIZE - huart1.hdmarx->Instance->NDTR;
			__enable_irq();
			if(dma_tail!=dma_head) {
				if(dma_head < dma_tail) {
					for(register size_t i=dma_head; i<dma_tail; i++)  {
						found = (found == 0 && buffer[i] == '\r') ? 1
								: (found == 1 && buffer[i] == '\n') ? 2
								: 0;
						msg_list[cur_msg][cur_msg_sz++]= buffer[i];

						if(found==2) {
							cur_msg = cur_msg == PARSER_MESSAGE_LIST_SIZE-1 ? 0 : cur_msg + 1;
	            		    memset(msg_list[cur_msg],0,PARSER_MESSAGE_SIZE);
	            		    cur_msg_sz=0;
						}
					}
				}
				else {
					for(register size_t i=dma_head; i<UART_DMA_BUFFER_SIZE; i++) {
						found = (found == 0 && buffer[i] == '\r') ? 1
								: (found == 1 && buffer[i] == '\n') ? 2
										: 0;
						msg_list[cur_msg][cur_msg_sz++]= buffer[i];

						if(found==2) {
							cur_msg = cur_msg == PARSER_MESSAGE_LIST_SIZE-1 ? 0 : cur_msg + 1;
							memset(msg_list[cur_msg],0,PARSER_MESSAGE_SIZE);
							cur_msg_sz=0;
						}
					}
					for(register size_t i=0; i<dma_tail; i++) {
						found = (found == 0 && buffer[i] == '\r') ? 1
								: (found == 1 && buffer[i] == '\n') ? 2
										: 0;

						msg_list[cur_msg][cur_msg_sz++]= buffer[i];

						if(found==2) {
							cur_msg = cur_msg == PARSER_MESSAGE_LIST_SIZE-1 ? 0 : cur_msg + 1;
							memset(msg_list[cur_msg],0,PARSER_MESSAGE_SIZE);
							cur_msg_sz=0;
						}
					}
				}
				if (found == 2) {
					vTaskNotifyGiveFromISR(uartReceptHandle, &xHigherPriorityTaskWoken);
					portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
				}
				dma_head=dma_tail;
			}
		}
		//while(dma_head!=(UART_DMA_BUFFER_SIZE- huart1.hdmarx->Instance->CNDTR));
		while(dma_head!=(UART_DMA_BUFFER_SIZE- huart1.hdmarx->Instance->NDTR));
		osDelay(10); // this should be the minimum time difference between each frame
	}
  /* USER CODE END UARTParser */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

static enum stateWifi  WifiSteps=WIFI_NO_MODULE;

void brewingTask(void const * argument) {
  /* USER CODE BEGIN UARTParser */

	/* Infinite loop */
	for(;;) {
		// Machine d'état brassage
		brewingLoop();
		// Affichage de l'état du wifi
		LEDWifiLoop(&WifiSteps);
		// On attends 10 ms
		osDelay(10);
	}
}

void UartReceptTask(void const * argument) {
	/* Infinite loop */
	static uint32_t thread_notification;

	for(;;) {
		thread_notification = ulTaskNotifyTake(pdTRUE,	portMAX_DELAY);
		if (thread_notification) {
			for (uint16_t j=0;j<PARSER_MESSAGE_LIST_SIZE;j++) {
				if (msg_list[j][0] != '\0') {
					AnalyseCommand((char *)msg_list[j]);
					memset(msg_list[j], 0, PARSER_MESSAGE_LIST_SIZE);
				}
			}
		}
	}
}

static inline uint8_t WaitForResp(uint16_t timeout) {
	int16_t cpt = timeout;
	while (ITEM_FOUND < 0 && cpt > 0) {
		cpt--;
		osDelay(100);
	}
	return ITEM_FOUND;
}

void AtProcessingTask(void const * argument) {
	uint8_t step=0;

	while (1) {
		switch (WifiSteps) {
		/*
		NO_MODULE=0,
			INITIALISATION,
			DISCONNECTED,
			CONNECTED_TO_WIFI,
			CONNECTED_TO_ENDPOINT,
			*/

			case WIFI_NO_MODULE:
				// On test la présence
				ITEM_FOUND = -1;
				sendAT("AT\r\n", 2, "OK\r\n", "ERROR\r\n");
				if (WaitForResp(50) == 0)
					WifiSteps = WIFI_INITIALISATION;
				else
					osDelay(500);
				break;

			case WIFI_INITIALISATION:
				switch (step) {
					case 0:
						ITEM_FOUND = -1;
						sendAT("AT+SLEEP=0\r\n", 2, "OK\r\n", "ERROR\r\n");
						if (WaitForResp(50) == 0)
							step = 1;
						else
							osDelay(500);
						break;

					case 1:
						ITEM_FOUND = -1;
						sendAT("AT+CWMODE=1\r\n", 2, "OK\r\n", "ERROR\r\n");
						if (WaitForResp(50) == 0) {
							step = 0;
							WifiSteps = WIFI_DISCONNECTED;
						}
						else
							osDelay(500);
						break;

					default:
						step = 0;
						break;
				}
				break;

				case WIFI_DISCONNECTED:
					ITEM_FOUND = -1;
					sendAT("AT+CWJAP=\"MarinMachine\",\"Bonjour01\"\r\n", 2, "WIFI GOT IP\r\n",  "FAIL\r\n");
					if (WaitForResp(100) == 0) {
						step = 0;
						WifiSteps = WIFI_CONNECTED;
					}
					else
						osDelay(5000);
					break;

				case WIFI_CONNECTED:
					switch (step) {
						case 0:
							ITEM_FOUND = -1;
							sendAT("AT+CIPSERVER=0,1\r\n", 2, "OK\r\n", "ERROR\r\n");
							if (WaitForResp(50) < 1) {
								step++;
							}
							else
								osDelay(500);
							break;

						case 1:
							ITEM_FOUND = -1;
							sendAT("AT+CIPMUX=1\r\n", 2, "OK\r\n", "ERROR\r\n");
							if (WaitForResp(50) < 1) {
								step++;
							}
							else
								osDelay(500);
							break;

						case 2:
							ITEM_FOUND = -1;
							sendAT("AT+CIPSERVERMAXCONN=1\r\n", 2, "OK\r\n", "ERROR\r\n");
							if (WaitForResp(50) < 1) {
								step++;
							}
							else
								osDelay(500);
							break;

						case 3:
							ITEM_FOUND = -1;
							//sendAT("AT+CIUPDATE\r\n", 2, "+CIPUPDATE:4\r\n", "ERROR\r\n");
							sendAT("AT+CIPSERVER=1,1300\r\n", 2, "OK\r\n", "ERROR\r\n");
							if (WaitForResp(600) < 1) {
								step++;
							}
							else
								osDelay(5000);
							break;

						default:
							osDelay(100);
							break;
					/*
						case 0:
							ITEM_FOUND = -1;
							sendAT("AT+CIPSTART=\"UDP\",\"192.168.64.255\",8800\r\n", 4, "CONNECT\r\n", "OK\r\n", "ALREADY CONNECTED\r\n\r\nERROR\r\n");
							if (WaitForResp(50) < 3) {
								step = 1;
							}
							else
								osDelay(500);
							break;

						case 1:
							ITEM_FOUND = -1;
							sendAT("AT+CIPSEND=0,8,\"192.168.64.255\",8800\r\n", 3, "OK\r\n", ">",  "ERROR\r\n");
							if (WaitForResp(50) < 2) {
								step = 2;
							}
							else
								osDelay(500);
							break;

						case 2:
							ITEM_FOUND = -1;
							sendAT("123456\r\n", 2, "SEND OK\r\n", "ERROR\r\n");
							if (WaitForResp(50) < 2) {
								step = 1;
							}
							else
								osDelay(500);
							break;
							*/
					}
					break;

			default:
				osDelay(100);
		}
		osDelay(100);
	}
}
/* USER CODE END Application */
