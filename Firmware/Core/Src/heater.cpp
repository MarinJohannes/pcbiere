/*
 * heater.cpp
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#include "heater.h"
#include "sensor.h"

extern TempSensor Temp1, Temp2;
extern Heater H1, H2;
extern uint8_t PAUSE;

void HeaterLoop(void) {
	H1.Play();
	H2.Play();
}

Heater::Heater(GPIO_TypeDef * reg, uint16_t pin, TempSensor * sensor) {
	this->pin = pin;
	this->reg = reg;
	this->cpt = 0;
	this->sensor = sensor;
	this->target_temp = 0;
	this->state = 0;
	this->reached = 0;
	this->duree = 0;
	this->done = 0;
	this->isOn = 0;
	this->unpause_state = 0;
	this->mem_pause = 2;
}

void Heater::AssignSensor(TempSensor * sensor) {
	this->sensor = sensor;
}

void Heater::ON(void) {
	this->state = 1;
}

void Heater::OFF(void) {
	this->state = 0;
}
void Heater::SetTarget(uint8_t target, uint8_t duree_minutes) {
	this->duree = duree_minutes*60000;
	this->reached = 0;
	this->target_temp = target;
	this->state = 2;
	this->done = 0;
}

void Heater::Play(void) {
	if (this->mem_pause != PAUSE) {
		// On bascule en pause/unpause
		if (PAUSE)
			this->unpause_state = this->state;
		else
			this->state = this->unpause_state;
		this->mem_pause = PAUSE;
	}

	switch (this->state) {
		case 0:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
			this->isOn = 0;
			break;

		case 1:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_SET);
			this->isOn = 1;
			break;

		case 2:
			if (this->done) {
				HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
				this->isOn = 0;
			}
			else if (this->sensor->GetValue() >= (this->target_temp+2)) {
				HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
				this->isOn = 0;
			}
			else if (this->sensor->GetValue() < (this->target_temp-2)) {
				HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_SET);
				this->isOn = 1;
			}

			if (!this->reached && this->sensor->GetValue() >= this->target_temp)
				this->reached = 1;
			if (this->reached && this->duree > 0)
				this->duree--;
			if (this->duree == 0 && this->reached)
				this->done = 1;
			break;
	}
}

void Heater::Toggle(void) {
	if (this->state > 0)
		this->state = 0;
	else
		this->state = 1;
}

uint8_t Heater::IsDone(void) {
	return this->done;
}
