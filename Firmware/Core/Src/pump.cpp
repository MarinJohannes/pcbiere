/*
 * pump.cpp
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#include "pump.h"
#include "sensor.h"

extern Pump P1, P2, P3;
extern uint8_t PAUSE;

void PumpLoop(void) {
	P1.Play();
	P2.Play();
	P3.Play();
}

Pump::Pump(GPIO_TypeDef * reg, uint16_t pin) {
	this->pin = pin;
	this->reg = reg;
	this->cpt = 0;
	this->state = 0;
	this->isOn = 0;
	this->unpause_state = 0;
	this->mem_pause = 2;
}

void Pump::ON(void) {
	this->state = 1;
}

void Pump::OFF(void) {
	this->state = 0;
}

void Pump::Play(void) {
	if (this->mem_pause != PAUSE) {
		// On bascule en pause/unpause
		if (PAUSE)
			this->unpause_state = this->state;
		else
			this->state = this->unpause_state;
		this->mem_pause = PAUSE;
	}

	switch (this->state) {
		case 0:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
			this->isOn = 0;
			break;

		case 1:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_SET);
			this->isOn = 1;
			break;
	}
}

void Pump::Toggle(void) {
	if (this->state > 0)
		this->state = 0;
	else
		this->state = 1;
}
