#include "sensor.h"
#include "config.h"

extern uint8_t TEMPERATURE_UNIT;
extern TempSensor Temp1, Temp2;
extern ADC_HandleTypeDef hadc1;

static void DS18B20_Write (uint8_t data, GPIO_TypeDef * reg, uint16_t pin);
static uint8_t DS18B20_Read(GPIO_TypeDef * reg, uint16_t pin);
static uint8_t DS18B20_Start(GPIO_TypeDef * reg, uint16_t pin);

TempSensor::TempSensor(GPIO_TypeDef * reg, uint16_t pin, enum TEMPERATURE_SENSOR_TYPE type) {
	this->pin = pin;
	this->reg = reg;
	this->type = type;
	this->pt100_alpha = 0.00385;
}

float TempSensor::GetValue(void) {
	return this->value;
}

void TempSensor::SetValue(uint32_t analog) {
	double an = 0;
	float Rt = 0;
	this->analog = analog;
	an = analog;

	switch (this->type) {
		case PT100:
			// T = ((Rt/R0)-1)/alpha avec Rt la résistance actuelle et alpha le coefficient (0.00385 par exemple)
			Rt = (4096 - this->analog)*(10000 / this->analog);
			this->value = ((Rt/100.0) - 1) / this->pt100_alpha;
			break;

		case KTY81:
			this->value = (3797.0 - 0.3494 * this->analog);
			break;

		case KTY83:
			this->value = (0.1067 * this->analog - 81.504);
			break;

		case NTC_5K:
			this->value = - 3022.66 + (5.93574*an) - 0.00459*(an*an) + 0.00000174717*(an*an*an) - 0.000000000326758*(an*an*an*an) + (0.0000000000000240906)*(an*an*an*an*an);
			break;

		case NTC_10K:
			this->value = - 242.086 + (0.610129*an) - 0.000591379*(an*an) + 0.000000283167*(an*an*an) - 0.000000000064967*(an*an*an*an) + (0.00000000000000576562)*(an*an*an*an*an);
			break;

		case DS18B20:
			this->value = analog/16;
			break;

		default:
			this->value = 0;
			break;
		}
}

void TempSensor::StartConv(void) {
	uint8_t Temp_byte1, Temp_byte2;
	// Appelé toutes les ms
	this->cpt = (this->cpt+1)%1000;
	if (this->type == DS18B20) {
		if (this->cpt == 1)
			DS18B20_Start(this->reg, this->pin);
		else if (this->cpt == 2) {
			DS18B20_Write (0xCC, this->reg, this->pin);  // skip ROM
			DS18B20_Write (0x44, this->reg, this->pin);  // convert t
		}
		else if (this->cpt == 802)
			DS18B20_Start(this->reg, this->pin);
		else if (this->cpt == 803) {
			DS18B20_Write (0xCC, this->reg, this->pin);  // skip ROM
			DS18B20_Write (0xBE, this->reg, this->pin);  // Read Scratch-pad
			Temp_byte1 = DS18B20_Read(this->reg, this->pin);
			Temp_byte2 = DS18B20_Read(this->reg, this->pin);
			this->SetValue(Temp_byte1 | (Temp_byte2<<8));
		}
	}
	else {
		switch (this->steps) {
			case 0:
				HAL_ADC_Start(&hadc1);
				HAL_ADC_PollForConversion(&hadc1, 1000);
				this->SetValue(HAL_ADC_GetValue(&hadc1));
				HAL_ADC_Stop(&hadc1);
				this->cpt = 0;
				this->steps = 1;
				break;

			case 1:
				if (this->cpt >= 999)
					this->steps = 0;
				break;
		}
	}
}

void TempSensor::ChangeType(enum TEMPERATURE_SENSOR_TYPE t) {
	this->type = t;
}

void TempSensor::ChangeType(uint8_t t) {
	this->type = (enum TEMPERATURE_SENSOR_TYPE)t;
}


static uint8_t DS18B20_Start(GPIO_TypeDef * reg, uint16_t pin) {
	uint8_t Response = 0;
	Set_Pin_Output(reg, pin);   // set the pin as output
	HAL_GPIO_WritePin (reg, pin, GPIO_PIN_RESET);  // pull the pin low
	delay(500);   // delay according to datasheet

	Set_Pin_Input(reg, pin);    // set the pin as input
	delay(60);    // delay according to datasheet

	if (!(HAL_GPIO_ReadPin (reg, pin))) Response = 1;    // if the pin is low i.e the presence pulse is detected
	else Response = -1;

	delay(50);
	return Response;
}

static void DS18B20_Write (uint8_t data, GPIO_TypeDef * reg, uint16_t pin) {
	Set_Pin_Output(reg, pin);  // set as output

	for (int i=0; i<8; i++) {
		if ((data & (1<<i))!=0) {  // if the bit is high
			// write 1
			Set_Pin_Output(reg, pin);  // set as output
			HAL_GPIO_WritePin (reg, pin, GPIO_PIN_RESET);  // pull the pin LOW
			delay (2);  // wait for 10 us

			Set_Pin_Input(reg, pin);  // set as input
			delay (78);  // wait for 60 us
		}

		else { // if the bit is low
			// write 0
			Set_Pin_Output(reg, pin);
			HAL_GPIO_WritePin (reg, pin, GPIO_PIN_RESET);  // pull the pin LOW
			delay (78);  // wait for 60 us
			Set_Pin_Input(reg, pin);
			delay (2);  // wait for 2 us
		}
	}
}

static uint8_t DS18B20_Read(GPIO_TypeDef * reg, uint16_t pin) {
	uint8_t value=0;
	Set_Pin_Input(reg, pin);

	for (int i=0;i<8;i++) {
		Set_Pin_Output (reg, pin);   // set as output
		HAL_GPIO_WritePin (reg, pin, GPIO_PIN_RESET);  // pull the data pin LOW
		delay (2);  // wait for 2 us

		Set_Pin_Input(reg, pin);  // set as input
		if (HAL_GPIO_ReadPin (reg, pin))  // if the pin is HIGH
			value |= 1<<i;  // read = 1
		delay (50);  // wait for 50 us
	}
	return value;
}
