/*
 * led.cpp
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */

#include "leds.h"
#include "config.h"
#include "main.h"

extern LED LED_INIT, LED_EMPAT, LED_HOUB, LED_REFROID, LED_WIFI;

void LEDLoop(void) {
	// Toutes les ms
	LED_INIT.Play();
	LED_EMPAT.Play();
	LED_HOUB.Play();
	LED_REFROID.Play();
	//LED_WIFI.Play();
}

LED::LED(GPIO_TypeDef * reg, uint16_t pin) {
	this->pin = pin;
	this->reg = reg;
	this->cpt = 0;
	this->state = 0;
	this->alpha = 0;
	this->T = 0;
}

void LED::Play(void) {
	static uint8_t i = 0, c=0;
	float a=0;

	switch (this->state) {
		case 0:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
			break;

		case 1:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_SET);
			break;

		case 2:
			this->cpt = (this->cpt+1)%this->T;
			if (this->cpt < this->alpha)
				HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
			break;

		case 3:
			// Une image = pwm sur 20. On calcule l'alpha
			a = 1.6*i - 0.032*i*i;
			// Gestion du PWM
			if (c < a)
				HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
			// On a le compteur qui va jusqu'à 20
			c = (c+1)%20;
			if (c == 0)
				i=(i+1)%50;
			break;

		default:
			HAL_GPIO_WritePin(this->reg, this->pin, GPIO_PIN_RESET);
			break;
	}
}

void LED::ON(void) {
	this->state = 1;
	this->cpt = 0;
}

void LED::OFF(void) {
	this->state = 0;
	this->cpt = 0;
}

void LED::Blink(float tau, uint16_t T) {
	this->alpha = uint16_t(tau*T);
	this->T = T;
	this->state = 2;
}

void LED::Sync(void) {
	this->cpt = 0;
}

void LED::Breath(void) {
	this->state = 3;
}
