/*
 * brew.cpp
 *
 *  Created on: 28 nov. 2022
 *      Author: MarinJ
 */
#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "tank.h"
#include "brew.h"
#include "leds.h"
#include "recipe.h"

// LEDs
extern LED LED_INIT, LED_EMPAT, LED_HOUB, LED_REFROID, LED_WIFI;
extern Recipe Recipe;

BrewSystem::BrewSystem(Tank * Tank) {
	this->MLT = Tank;
}

BrewSystem::BrewSystem(Tank * MLT, Tank * HLT, Tank * BK) {
	this->MLT = MLT;
	this->HLT = HLT;
	this->BK = BK;
}

void BrewSystem::Loop(void) {
	switch (this->brewType) {
		// Brew In a Bag
		case BIAB:
			if (this->MLT->pump != NULL && (this->state == EMPATAGE || this->state == HOUBLONNAGE))
				this->MLT->pump->ON();
			else
				this->MLT->pump->OFF();

			switch (this->state) {
				case INACTIVE:
					// Pour démarrer, pour le moment on veut simplement presser le bouton pause
					if (this->Next) {
						LED_INIT.Breath();
						LED_EMPAT.OFF();
						LED_HOUB.OFF();
						LED_REFROID.OFF();
						this->state = INITIALISATION;
					}
					break;

				case INITIALISATION:
					if (this->Next) {
						LED_INIT.ON();
						LED_EMPAT.Breath();
						LED_HOUB.OFF();
						LED_REFROID.OFF();
						this->MLT->heater->SetTarget(Recipe.GetMaltStepTemp(), Recipe.GetMaltStepDuration());
						this->state = EMPATAGE;
					}
					break;

				case EMPATAGE:
					// Etape terminee, on passe à la suivante
					if (this->MLT->heater->IsDone()) {
						Recipe.NextMaltStep();
						this->MLT->heater->SetTarget(Recipe.GetMaltStepTemp(), Recipe.GetMaltStepDuration());
					}
					// Si on a terminé
					if (Recipe.IsMaltDone()) {
						LED_INIT.ON();
						LED_EMPAT.ON();
						LED_HOUB.Breath();
						LED_REFROID.OFF();
						this->MLT->heater->SetTarget(100, 1); // On porte à ébullition
						this->state = HOUBLONNAGE;
					}
					break;

				case HOUBLONNAGE:
					switch (Recipe.hopStep) {
						case FIRST_BOIL:
							if (this->MLT->heater->IsDone()) {
								LED_HOUB.Blink(0.5, 500);
								Recipe.hopStep = WAITING_FOR_HOP;
							}
							break;

						case BOILING:
							if (this->MLT->heater->IsDone()) {
								if (Recipe.IsHopDone()) {
									// Il n'y a plus de houblon à mettre
									LED_INIT.ON();
									LED_EMPAT.ON();
									LED_HOUB.ON();
									LED_REFROID.Breath();
									this->MLT->heater->SetTarget(Recipe.GetYeastTemp(), 1);
									this->state = REFROIDISSEMENT;
								}
								else {
									// On a terminé, on BLINK parce qu'on attend du houblon
									Recipe.NextHopStep();
									LED_HOUB.Blink(0.5, 500);
									Recipe.hopStep = WAITING_FOR_HOP;
								}
							}
							break;

						case WAITING_FOR_HOP:
							if (this->Next) {
								LED_HOUB.Breath();
								this->MLT->heater->SetTarget(100, Recipe.GetHopStepDuration()); // On porte à ébullition
								Recipe.hopStep = BOILING;
							}
							break;
					}
					break;

				case REFROIDISSEMENT:
					if (this->MLT->heater->IsDone()) {
						LED_INIT.ON();
						LED_EMPAT.ON();
						LED_HOUB.ON();
						LED_REFROID.ON();
					}
					break;

				default:
					break;
			}
			break;

		case HERMS:
			break;
	}
	this->Next = 0;
}
