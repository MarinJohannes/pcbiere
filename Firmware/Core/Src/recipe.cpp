/*
 * recipe.cpp
 *
 *  Created on: Oct 28, 2022
 *      Author: Marin
 */
#include "recipe.h"
#include <algorithm>

Recipe::Recipe(void) {
	this->n_step = 0;
	this->i_step = 0;
	this->h_step = 0;
	this->n_hop = 0;
	this->hop_duration = 0;
	this->hopStep = FIRST_BOIL;
}

void Recipe::AddMaltStep(uint8_t temp, uint8_t duree) {
	this->Paliers[this->n_step].temp = temp;
	this->Paliers[this->n_step].duree = duree;
	this->Paliers[this->n_step].done = 0;
	this->n_step++;
}

void Recipe::NextMaltStep(void) {
	this->Paliers[this->i_step].done = 1;
	if (this->i_step < this->n_step)
		this->i_step++;
}

uint8_t Recipe::IsMaltDone(void) {
	if (this->i_step == this->n_step && this->Paliers[this->i_step].done)
		return 1;
	return 0;
}

uint8_t Recipe::IsMaltStepDone(void) {
	if (this->Paliers[this->i_step].done)
		return 1;
	return 0;
}

uint8_t Recipe::GetMaltStepTemp(void) {
	return this->Paliers[this->i_step].temp;
}

uint8_t Recipe::GetMaltStepDuration(void) {
	if (this->i_step < this->n_step)
		return this->Paliers[this->i_step].duree;
	else
		return 0;
}

void Recipe::AddHop(enum HOP_LIST hop, uint8_t duree) {
	this->Hops[this->n_hop].duree = duree;
	this->Hops[this->n_hop].hop = hop;
	this->n_hop++;
}

static bool SORT_DESC(int i, int j) {
   return i > j;    // retourne 1 si i>j sinon 0
}

void Recipe::CalculateHops(void) {
	uint32_t du[10] = {0};
	uint8_t i;
	for (i=0; i<10; i++) {
		if (this->Hops[i].duree > 0) {
			du[i] = this->Hops[i].duree;
		}
		else
			break;
	}
	std::sort(du, du+i, SORT_DESC); // ou i-1?
	for (int j=0;j<i-1;j++) {
		hopSteps[j] = du[j] - du[j+1];
	}
}

void Recipe::NextHopStep(void) {
	this->h_step++;
}

uint8_t Recipe::GetHopStepDuration(void) {
	if (this->h_step < 10)
		return this->hopSteps[this->h_step];
	return 0;
}

uint8_t Recipe::IsHopDone(void) {
	if (this->hopSteps[this->h_step] == 0)
		return 1;
	return 0;
}

void Recipe::SetYeastTemp(uint8_t temp) {
	this->yeast_temp = temp;
}

uint8_t Recipe::GetYeastTemp(void) {
	return this->yeast_temp;
}
