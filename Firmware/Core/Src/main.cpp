/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "FreeRTOS.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "eeprom.h"
#include "config.h"
#include "sensor.h"
#include "button.h"
#include "leds.h"
#include "heater.h"
#include "recipe.h"
#include "hops.h"
#include "pump.h"
#include "output.h"
#include "tank.h"
#include "brew.h"
#include <cstdlib>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
enum stateMachine sMachine = INACTIVE;
uint8_t PAUSE = 0;

extern UART_HandleTypeDef huart1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// DECLARATIONS /////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CAPTEURS
uint8_t TEMPERATURE_UNIT = DEGRE_CELCIUS;

TempSensor Temp1(TEMP1_GPIO_Port, TEMP1_Pin, NTC_5K);
TempSensor Temp2(TEMP2_GPIO_Port, TEMP2_Pin, NTC_5K);

// HEATER
Heater H1(IN_H1_GPIO_Port, IN_H1_Pin, &Temp1);
Heater H2(IN_H2_GPIO_Port, IN_H2_Pin, &Temp2);

// POMPES
Pump P1(IN_P1_GPIO_Port, IN_P1_Pin);
Pump P2(IN_P2_GPIO_Port, IN_P2_Pin);
Pump P3(IN_P3_GPIO_Port, IN_P3_Pin);

// Sorties 12V
Output O1(IN_OUT1_GPIO_Port, IN_OUT1_Pin);
Output O2(IN_OUT2C9_GPIO_Port, IN_OUT2C9_Pin);

// LEDs
LED LED_INIT(LED_INIT_GPIO_Port, LED_INIT_Pin);
LED LED_EMPAT(LED_EMPAT_GPIO_Port, LED_EMPAT_Pin);
LED LED_HOUB(LED_HOUB_GPIO_Port, LED_HOUB_Pin);
LED LED_REFROID(LED_REFROID_GPIO_Port, LED_REFROID_Pin);
LED LED_WIFI(LED_WIFI_GPIO_Port, LED_WIFI_Pin);

// Boutons
Button BP_H1(SW_H1_GPIO_Port, SW_H1_Pin);
Button BP_H2(SW_H2_GPIO_Port, SW_H2_Pin);
Button BP_P1(SW_P1_GPIO_Port, SW_P1_Pin);
Button BP_P2(SW_P2_GPIO_Port, SW_P2_Pin);
Button BP_P3(SW_P3_GPIO_Port, SW_P3_Pin);
Button BP_MODE(SW_MODE_GPIO_Port, SW_MODE_Pin);
Button BP_O1(SW_O1_GPIO_Port, SW_O1_Pin);
Button BP_O2(SW_O2_GPIO_Port, SW_O2_Pin);
Button BP_PAUSE(SW_PAUSE_GPIO_Port, SW_PAUSE_Pin);

// Recipe
Recipe Recipe;

MACHINE_CONFIG_t config = {0};

// Tank
Tank * tanks[3];
BrewSystem * Brew;

uint8_t buffer[UART_DMA_BUFFER_SIZE];

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	Recipe.AddMaltStep(35, 1);	// 68°C pendant 70min
	//Recipe.AddMaltStep(55, 1);	// 78°C pendant 10min
	Recipe.AddHop(STYRIAN_GOLDINGS, 10);
	Recipe.AddHop(NORTHERN_BREWER, 80);
	Recipe.CalculateHops();
	Recipe.SetYeastTemp(29);

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
  DWT->CYCCNT = 0;
  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
 // HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
  HAL_TIM_Base_Start_IT(&htim3);

  // Initialisation EEPROM
  /* Unlock the Flash Program Erase controller */
  HAL_FLASH_Unlock();
  /* EEPROM Init */

  if (EE_Init() != HAL_OK) {
	  Error_Handler();
  }

  LoadConfig(&config);
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

  /* Start scheduler */
  osKernelStart();
  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

  }
    /* USER CODE BEGIN 3 */
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
uint8_t pauseRE;
void brewingLoop(void) {
	pauseRE = BP_PAUSE.isRisingEdge();
	if (pauseRE)
		Brew->Next = 1;

	if (BP_PAUSE.HowLong() >= 2000) {
		PAUSE = 1 - PAUSE;
		BP_PAUSE.Reset();
		if (PAUSE) {
			// On passe toutes les LEDs en blink
			LED_INIT.Blink(0.5, 500);
			LED_EMPAT.Blink(0.5, 500);
			LED_HOUB.Blink(0.5, 500);
			LED_REFROID.Blink(0.5, 500);
			LED_INIT.Sync();
			LED_EMPAT.Sync();
			LED_HOUB.Sync();
			LED_REFROID.Sync();
			H1.OFF();
			H2.OFF();
			P1.OFF();
			P2.OFF();
			P3.OFF();
			BP_H1.Reset();
			BP_H2.Reset();
			BP_P1.Reset();
			BP_P2.Reset();
			BP_P3.Reset();
		}
		else {
			LED_INIT.OFF();
			LED_EMPAT.OFF();
			LED_HOUB.OFF();
			LED_REFROID.OFF();
		}
	}
	// Sorties 12V, toujours en mode manu
	if (BP_O1.isRisingEdge())
		O1.Toggle();
	if (BP_O2.isRisingEdge())
		O2.Toggle();

	if (PAUSE) {
		// MODE MANUEL
		if (BP_H1.isRisingEdge())
			H1.Toggle();
		if (BP_H2.isRisingEdge())
			H2.Toggle();
		if (BP_P1.isRisingEdge())
			P1.Toggle();
		if (BP_P2.isRisingEdge())
			P2.Toggle();
		if (BP_P3.isRisingEdge())
			P3.Toggle();
	}
	else {
		Brew->Loop();
	}
}

void LEDWifiLoop(enum stateWifi * WifiSteps) {
	switch (*WifiSteps) {
		case WIFI_NO_MODULE:
		case WIFI_DISCONNECTED:
			LED_WIFI.OFF();
			break;

		case WIFI_CONNECTED:
			LED_WIFI.Blink(0.75, 500);
			break;

		case WIFI_INITIALISATION:
			LED_WIFI.Blink(0.5, 500);
			break;

		case WIFI_CONNECTED_TO_ENDPOINT:
			LED_WIFI.ON();
			break;
	}
}

void delay(volatile uint32_t au32_microseconds) {

  uint32_t au32_initial_ticks = DWT->CYCCNT;
  uint32_t au32_ticks = (HAL_RCC_GetHCLKFreq() / 1000000);
  au32_microseconds *= au32_ticks;
  while ((DWT->CYCCNT - au32_initial_ticks) < au32_microseconds-au32_ticks);

	/*
	__HAL_TIM_SET_COUNTER(&htim5, 0);
	while ((__HAL_TIM_GET_COUNTER(&htim5))<au32_microseconds);
	*/
}

void Set_Pin_Output(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
	GPIO_InitTypeDef sG = {0};
	sG.Pin = GPIO_Pin;
	sG.Mode = GPIO_MODE_OUTPUT_OD;
	sG.Pull = GPIO_NOPULL;
	sG.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOx, &sG);
}

void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
	GPIO_InitTypeDef sG = {0};
	sG.Pin = GPIO_Pin;
	sG.Mode = GPIO_MODE_INPUT;
	sG.Pull = GPIO_NOPULL;
	sG.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOx, &sG);
}

void LoadConfig(MACHINE_CONFIG_t * config) {
	uint16_t * data = (uint16_t *)&config;
	Heater * hs[3] = {NULL, &H1, &H2};
	Pump * ps[3] = {NULL, &P1, &P2};
	TempSensor * ts[3] = {NULL, &Temp1, &Temp2};

	if (data != NULL) {
		for (uint16_t i=0; i<sizeof(MACHINE_CONFIG_t); i++) {
			if ((EE_ReadVariable(i,  &data[i])) != HAL_OK)
				Error_Handler();
		}
	}
	else {
		Error_Handler();
	}
	// On charge les trois cuves (dont celles avec NULL)
	for (int i=0; i<3; i++)
			tanks[i] = new Tank(hs[config->heaters[i]], ts[config->tempSensors[i]], ps[config->pumps[i]]);
	// On assigne au système les cuves
	/*
	BrewSystem b(tanks[0], tanks[1], tanks[2]);
	Brew = &b;
	*/
	Brew = new BrewSystem(tanks[0], tanks[1], tanks[2]);
	// On détermine l'architecture (BIAB si un seul heater)
	if (tanks[1]->heater != NULL)
		Brew->brewType =HERMS;
	else
		Brew->brewType = BIAB;

	for (int i=1; i<3; i++)
		ts[i]->ChangeType(config->tempSensorsTypes[i-1]);
}

void SaveConfig(MACHINE_CONFIG_t * config) {
	uint16_t * data = (uint16_t *)&config;
	if (data != NULL) {
		for (uint16_t i=0; i<sizeof(MACHINE_CONFIG_t); i++) {
			if (EE_WriteVariable(i, data[i]) != HAL_OK)
				Error_Handler();
		}
	}
	else {
		Error_Handler();
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
