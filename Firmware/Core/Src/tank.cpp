/*
 * tank.cpp
 *
 *  Created on: 28 nov. 2022
 *      Author: MarinJ
 */
#include <stdint.h>
#include "stm32f401xc.h"
#include "config.h"
#include "heater.h"
#include "sensor.h"
#include "pump.h"
#include "tank.h"

Tank::Tank(Heater * h, TempSensor * s, Pump * p) {
	if (h != NULL)
		this->heater = h;
	if (s != NULL)
		this->sensor = s;
	if (p != NULL)
		this->pump = p;
}


