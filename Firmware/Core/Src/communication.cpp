/*
 * communication.cpp
 *
 *  Created on: 1 nov. 2022
 *      Author: Marin
 */

#include "communication.h"
#include "config.h"
#include "sensor.h"
#include "button.h"
#include "leds.h"
#include "heater.h"
#include "recipe.h"
#include "hops.h"
#include "pump.h"
#include "output.h"
#include "usart.h"
#include "brew.h"
#include "tank.h"
#include <string.h>


extern uint8_t TEMPERATURE_UNIT;
extern TempSensor Temp1, Temp2;
extern Heater H1, H2;
extern Pump P1, P2, P3;
extern Output O1, O2;
extern LED LED_INIT, LED_EMPAT, LED_HOUB, LED_REFROID, LED_WIFI;
extern Button BP_H1, BP_H2, BP_P1, BP_P2, BP_P3, BP_MODE, BP_O1, BP_O2, BP_PAUSE;
extern Recipe Recipe;
extern enum stateMachine sMachine;
extern uint8_t PAUSE;
extern BrewSystem * Brew;

char * GenerateHearbeat(void) {
	static char trame[8];
	trame[0] = PAUSE;
	trame[1] = (char)sMachine;
	trame[2] = (H1.isOn<<7) + (H2.isOn<<6) + (P1.isOn<<5) + (P2.isOn<<4) + (P3.isOn<<3) + (O1.isOn<<2) + (O2.isOn<<1);
	return trame;
}


void AnalyseCommand(char * str) {
	if (strstr(str, "=CONFIG")) {
		LED_WIFI.Blink(0.5, 2500);
	}
	else if (strstr(str, "=RECIPE")) {
		if (strstr(str, "START"))
			Brew->Next = 1;
		if (strstr(str, "STOP"))
			return;
		if (strstr(str, "PAUSE"))
			return;
	}
}

