#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>

void read_sensor_data(void * parameter);

const char* ssid = "MarinMachine";
const char* password = "Bonjour01";

// Web server running on port 80
AsyncWebServer server(80);

void setup() {
  Serial.begin(115200);
  delay(1000);
  /* Connexion au réseau WiFi. Il faut faire en sorte qu'en cas de non connexion on puisse le reconfigurer par bluetooth ou par point d'accès web. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.print("Connected to IP : ");
  Serial.println(WiFi.localIP());

  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(200, "text/plain", "192 C");
  });
  server.on("/pressure", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(200, "text/plain", "1250 mP");
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(200, "text/plain", "75%");
  });
  server.on("/env", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(200, "text/plain", "test");
  });
  AsyncElegantOTA.begin(&server);

  //xTaskCreate(read_sensor_data, "Read sensor data", 1000, NULL, 1, NULL);

  server.begin();
}


void loop() {
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.reconnect();
    while (WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(100);
    }
  }
}

void read_sensor_data(void * parameter) {
  for (;;) {
    // delay the task
    vTaskDelay(60000 / portTICK_PERIOD_MS);
  }
}
