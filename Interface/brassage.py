import ttkbootstrap as ttk
from ttkbootstrap.constants import *


class Brassage(ttk.Frame):
    def __init__(self, master, tab):
        self.master = master
        self.frame = ttk.Frame(self.master)

        ttk.Label(tab, text="Avancement global", font='-size 30').pack(side='top', fill='x', pady=5)

        self.empatage_bar = ttk.Progressbar(tab, bootstyle="striped")
        self.empatage_bar.pack(fill=BOTH, expand=YES, padx=10, pady=10)
        self.empatage_bar.configure(value=60)

    def new_window(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = Empatage(self.newWindow)