import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from accueil import Accueil
from brassage import Brassage

class MainApplication(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        tabControl = ttk.Notebook(self)
        tab1 = ttk.Frame(tabControl)
        tab2 = ttk.Frame(tabControl)
        tab3 = ttk.Frame(tabControl)
        tab4 = ttk.Frame(tabControl)
        self.accueil = Accueil(self, tab1)
        self.recettes = Accueil(self, tab2)
        self.brassage = Brassage(self, tab3)
        self.configuration = Accueil(self, tab4)
        tabControl.add(tab1, text='Accueil')
        tabControl.add(tab2, text='Recettes')
        tabControl.add(tab3, text='Brassage')
        tabControl.add(tab4, text='Configuration')
        tabControl.pack(expand = 1, fill ="both")
        
        

if __name__ == "__main__":
    root = ttk.Window(size=(800, 600))
    root.title("PCBière Tools")
    MainApplication(root).pack(side="top", fill="both", expand=True)
    root.mainloop()