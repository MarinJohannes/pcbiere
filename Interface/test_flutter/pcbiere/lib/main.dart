import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

class BrewingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Brewing'),
      ),
      body: Column(
        children: [
          Text("Température S1 : 23.7°C"),
          Text("Température S2 : 24.1°C"),
        ]
      ),
    );
  }
}

const List<Map<String, String>> recipes = [
  {
    'id': '1',
    'title': 'Indian Pale Ale 1',
    'type': 'IPA',
    'author': 'Marin',
  },
  {
    'id': '2',
    'title': 'Indian Pale Ale 2',
    'type': 'IPA',
    'author': 'Marin',
  },
  {
    'id': '3',
    'title': 'Biere de noel',
    'type': 'Autres',
    'author': 'Marin',
  },
];

class RecipesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recipes'),
      ),
      body: ListView(
        children: recipes
            .map(
              (recipe) => ListTile(
                title: Text(recipe['title']!),
                subtitle: Text(recipe['author']!),
                onTap: () => context.beamToNamed('/recipes/${recipe['id']}'),
              ),
            )
            .toList(),
      ),
    );
  }
}

class RecipesDetailsScreen extends StatelessWidget {
  const RecipesDetailsScreen({required this.recipe});
  final Map<String, String> recipe;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(recipe['title']!),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          Text('Author: ${recipe['author']!}'),
          Text('Type: ${recipe['type']!}'),
          Text("Bref faut détailler la recette ici avec un peu de style"),
        ]),
      ),
    );
  }
}

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Column(
        children: [
          Text("Paramétrage de la PCBière"), 
          Text("En gros si on est en BIAB / HERMS / RIMS"),
          Text("Quel heater sur quelle cuve, associé à quel capteur, etc"),
          Text("Et bien sûr les types de capteurs"),
        ]
      ),
    );
  }
}

// LOCATIONS
class BrewingLocation extends BeamLocation<BeamState> {
  @override
  List<String> get pathPatterns => ['/brewing'];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) => [
        BeamPage(
          key: ValueKey('brewing'),
          title: 'Brewing',
          type: BeamPageType.noTransition,
          child: BrewingScreen(),
        ),
      ];
}

class RecipesLocation extends BeamLocation<BeamState> {
  @override
  List<String> get pathPatterns => ['/recipes/:recipeId'];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) => [
        BeamPage(
          key: ValueKey('recipes'),
          title: 'Recipes',
          type: BeamPageType.noTransition,
          child: RecipesScreen(),
        ),
        if (state.pathParameters.containsKey('recipeId'))
          BeamPage(
            key: ValueKey('recipe-${state.pathParameters['recipeId']}'),
            title: recipes.firstWhere((recipe) =>
                recipe['id'] == state.pathParameters['recipeId'])['title'],
            child: RecipesDetailsScreen(
              recipe: recipes.firstWhere(
                  (recipe) => recipe['id'] == state.pathParameters['recipeId']),
            ),
          ),
      ];
}

class SettingsLocation extends BeamLocation<BeamState> {
  @override
  List<String> get pathPatterns => ['/settings'];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) => [
        BeamPage(
          key: ValueKey('settings'),
          title: 'Settings',
          type: BeamPageType.noTransition,
          child: SettingsScreen(),
        ),
      ];
}

// APP
class BottomNavigationBarWidget extends StatefulWidget {
  BottomNavigationBarWidget({required this.beamerKey});

  final GlobalKey<BeamerState> beamerKey;

  @override
  _BottomNavigationBarWidgetState createState() =>
      _BottomNavigationBarWidgetState();
}

class _BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget> {
  late BeamerDelegate _beamerDelegate;
  int _currentIndex = 0;

  void _setStateListener() => setState(() {});

  @override
  void initState() {
    super.initState();
    _beamerDelegate = widget.beamerKey.currentState!.routerDelegate;
    _beamerDelegate.addListener(_setStateListener);
  }

  @override
  Widget build(BuildContext context) {
    _currentIndex =
        _beamerDelegate.currentBeamLocation is BrewingLocation ? 0 : (_beamerDelegate.currentBeamLocation is RecipesLocation ? 1 : 2);
    return BottomNavigationBar(
      currentIndex: _currentIndex,
      items: [
        BottomNavigationBarItem(label: 'Brewing', icon: Icon(Icons.sports_bar)),
        BottomNavigationBarItem(label: 'Recipes', icon: Icon(Icons.article)),
        BottomNavigationBarItem(label: 'Settings', icon: Icon(Icons.settings)),
      ],
      onTap: (index) => _beamerDelegate.beamToNamed(
        index == 0 ? '/brewing' : (index == 1 ? '/recipes' : '/settings'),
      ),
    );
  }

  @override
  void dispose() {
    _beamerDelegate.removeListener(_setStateListener);
    super.dispose();
  }
}

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  final _beamerKey = GlobalKey<BeamerState>();
  final _routerDelegate = BeamerDelegate(
    locationBuilder: BeamerLocationBuilder(
      beamLocations: [
        BrewingLocation(),
        RecipesLocation(),
        SettingsLocation(),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Beamer(
        key: _beamerKey,
        routerDelegate: _routerDelegate,
      ),
      bottomNavigationBar: BottomNavigationBarWidget(
        beamerKey: _beamerKey,
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  final routerDelegate = BeamerDelegate(
    initialPath: '/brewing',
    locationBuilder: RoutesLocationBuilder(
      routes: {
        '*': (context, state, data) => HomeScreen(),
      },
    ),
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate: routerDelegate,
      routeInformationParser: BeamerParser(),
    );
  }
}

void main() => runApp(MyApp());

